class UserService {
	// приватные поля
	#username;
	#password;
	// конструктор
	constructor(username, password) {
		this.#username = username;
		this.#password = password;
	}
	// геттер для имени
	get username() {
		return this.#username;
	}
	// геттер для пароля
	get password() {
		throw "!!! You are not allowed to get password !!!";
	}
		
	// асинхронный метод аутентификации пользователя
	async authenticateUser() {
		const domainAPI = 'http://localhost:8000';
		const res = await fetch(domainAPI+'/auth', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					username: this.#username,
					password: this.#password
				})
			});
		return await res.json();
	}

}

// отправка данных из формы (jquery)
$('form #login').click( () => {
	const username = $('#username').val();
	const password = $('#password').val();

	const res = new UserService(username, password).authenticateUser();

	res.then(res => {
		if (res == true) {
			document.location.href = '/home'
		} else {
			console.log("error")
		}
	});
})
