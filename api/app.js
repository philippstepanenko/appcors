const express = require('express')
const app = express()
const port = 8000

const cors = require('cors')
const corsOptions = {
	origin: 'http://front',
	optionsSuccessStatus: 200
}

app.use(cors(corsOptions))
app.use(express.json())

app.post('/auth', function (req,res,next){
	if (req.body.username == "admin" &&
		req.body.password == 42){
		res.send(true)
	}else{
		res.send(false)
	}
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})